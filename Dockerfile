# simple-shortener Dockerfile
# Produces an image (Debian-based) with all required facilities to run simple-shortener

FROM debian:buster-slim

LABEL maintainer='Jessie Hildebrandt <jessieh@jessieh.net>'

ENV PORT 80
WORKDIR /simple-shortener

# Install required packages for building luarocks and lua rocks
RUN apt-get update && apt-get install --no-install-recommends -y \
    ca-certificates \
    cmake \
    gcc \
    git \
    luajit \
    liblua5.1-0-dev \
    libqrencode-dev \
    libsqlite3-dev \
    libssl-dev \
    make \
    tar \
    unzip \
    wget \
    && rm -rf /var/lib/apt/lists/*

# Fetch, build, and install luarocks
RUN wget https://luarocks.org/releases/luarocks-3.7.0.tar.gz && \
    tar xf luarocks-3.7.0.tar.gz && \
    cd luarocks-3.7.0 && \
    ./configure && make && make install && \
    cd .. && \
    rm -rf luarocks-3.7.0.tar.gz luarocks-3.7.0/

# Install required rocks
RUN luarocks install basexx 0.4.1-1 && \
    luarocks install ezenv 1.2-0 && \
    luarocks install fennel 0.10.0-1 && \
    luarocks install hashids 1.0.5-1 && \
    luarocks install lume 2.3.0-0 && \
    luarocks install otp 0.1-6 && \
    luarocks install qrprinter 1.1-0 && \
    luarocks install sqlite v1.2.0-0 && \
    luarocks install turbo 2.1-2

COPY ./src .

ENTRYPOINT ["luajit", "init.lua"]
