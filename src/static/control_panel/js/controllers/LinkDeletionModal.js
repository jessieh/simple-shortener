/**
 * controllers/LinkDeletionModal.js
 *
 * @file Provides controller for LinkDeletionModal component
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { ShortenLinkCard } from '../interfaces/ShortenLinkCard.js';
import { ShortenedLinksPanel } from '../interfaces/ShortenedLinksPanel.js';

import { sleep } from '../utils/utils.js';

/* -------------------------------------------------------------------------- */
/* Controller implementation */

/**
 * Controller for LinkDeletionModal component
 */
const LinkDeletionModal = {

    /* ---------------------------------- */
    /* init */

    /**
     * Initialize the controller
     */
    init()
    {

        LinkDeletionModal.linkDest = '';

        LinkDeletionModal.deletionCountdown = 3;
        LinkDeletionModal.deletionTimer = null;

        LinkDeletionModal.isCountingDown = true;
        LinkDeletionModal.isWaiting = true;

    },

    /* ---------------------------------- */
    /* reset */

    /**
     * Reset the controller
     */
    reset()
    {
        if ( LinkDeletionModal.deletionTimer !== null )
        {
            clearTimeout( LinkDeletionModal.deletionTimer );
        }
        LinkDeletionModal.init();
    },

    /* ---------------------------------- */
    /* load */

    /**
     * Load required information about the selected link from the API and populate it into the modal
     *
     * @param {string} linkID - The ID of the link for which the information will be loaded
     */
    async load( linkID )
    {

        // Load the link's data into the controller
        try
        {
            const response = await m.request( `/shorten/api/links/${ linkID }` );
            const link = response.find( link => link.link_id === linkID );
            LinkDeletionModal.linkDest = link.link_dest;
        }
        catch
        {
            m.route.set( '/app' );
        }

        // Once the link's data has been found, start a countdown timer to enable the "Confirm" button
        const countdown = () => {
            if ( !( LinkDeletionModal.deletionCountdown-- > 0 ) )
            {
                LinkDeletionModal.isCountingDown = false;
            }
            else
            {
                LinkDeletionModal.deletionTimer = setTimeout( countdown, 1000 );
            }
            m.redraw();
        }
        countdown();

        // We're done our work
        LinkDeletionModal.isWaiting = false;
        m.redraw();

    },

    /* ---------------------------------- */
    /* close */

    /**
     * Close the link deletion modal
     */
    close()
    {
        m.route.set( '/app' );
    },

    /* ---------------------------------- */
    /* openOptionsModal */

    /**
     * Go back to the link options modal for the selected link
     *
     * @param {string} linkID - The ID of the link for which to open the options modal
     */
    openOptionsModal( linkID )
    {
        m.route.set( '/app/options/:linkID', { linkID: linkID } );
    },

    /* ---------------------------------- */
    /* tryDelete */

    /**
     * Attempt to delete the selected link
     *
     * @param {string} linkID - The ID of the link that should be deleted
     */
    async tryDelete( linkID )
    {

        // Signal to the component that activity is occurring
        LinkDeletionModal.isWaiting = true;
        m.redraw();

        // Prevent the modal from blinking away too quickly
        await sleep( 150 );

        // Attempt to delete the link by submitting a request to the API
        try
        {
            const response = await m.request( {
                method: 'DELETE',
                url: `/shorten/api/links/${ linkID }`,
                background: true
            } );
        }
        catch
        {}

        // Refresh the "Shorten a link" and "Shortened links" UI components
        ShortenLinkCard.refresh();
        ShortenedLinksPanel.refresh();

        // Close the modal
        LinkDeletionModal.close();

    }

}

/* -------------------------------------------------------------------------- */
/* Export */

export { LinkDeletionModal };
